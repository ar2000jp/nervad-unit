# nervad-unit

Nerva (XNV) daemon systemd unit file with as many systemd sandboxing options enabled as possible.

# Installation

Examples are for Debian 11.

1. Clone the repo and copy the unit file to the systemd unit files folder.
   For example:-
    ```
    git clone git@gitlab.com:ar2000jp/nervad-unit.git
    cd nervad
    sudo cp nervad.service /etc/systemd/system/
    ```
2. Download and extract the nervad binary to "/opt/nerva/".
   For example:-
   ```
   sudo mkdir -p /opt/nerva/
   cd /opt/nerva/
   sudo unzip ~/Downloads/nerva-*_linux_minimal.zip
   sudo chmod a+x /opt/nerva/nervad
   ```
3. Create nervad config file in the nervad systemd unit config folder (usually "/etc/nerva").
   For example:-
   ```
   sudo mkdir -p /etc/nerva/
   sudo touch /etc/nerva/nerva.conf
   ```

4. Create "nerva" linux user.
   For example:-
   ```
   sudo useradd -M -U -r -d /opt/nerva/ -s /usr/sbin/nologin nerva
   ```

5. Ask systemd daemon to reload
   ```
   sudo systemctl daemon-reload
   ```

# Usage

- To start the daemon, do:
   ```
   sudo systemctl start nervad.service
   ```

- To stop the daemon, do:
    ```
    sudo systemctl stop nervad.service
    ```

- To enable daemon start on boot, do:
    ```
    sudo systemctl enable nervad.service
    ```

- To see the daemon logs, do:
   ```
   sudo journalctl -f --unit=nervad
   ```
